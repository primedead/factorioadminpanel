﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FactorioAdminPanel.Controllers
{
    public class ApiControler : Controller
    {
        public string GetPostData()
        {
            byte[] b = new byte[Request.Body.Length];
            Request.Body.Read(b, 0, b.Length);
            int i = Request.ContentType.IndexOf("charset=")+8;
            string ret = "";
            switch (Request.ContentType.Substring(i))
            {
                case "utf-8":
                    ret = Encoding.UTF8.GetString(b);
                    break;
                case "utf-16":
                    ret = Encoding.Unicode.GetString(b);
                    break;
                case "unicode":
                    ret = Encoding.Unicode.GetString(b);
                    break;
                default:
                    break;
            }
            return ret;
        }
    }
}
